import {
    LOGIN_USER,
    AUTH_FAILURE,
    LOGIN_USER_SUCCESS,
    GET_USER_PROFILE,
    GET_USER_PROFILE_SUCCESS,
    LOGOUT_USER,
    REGISTER_USER,
    REGISTER_USER_SUCCESS,
    LOGOUT_USER_SUCCESS,
    FORGOT_PASSWORD,
    FORGOT_PASSWORD_SUCCESS
} from "../actions";
export const loginUser = (user, history) => ({
    type: LOGIN_USER,
    payload: { user, history }
  });
  export const loginUserSuccess = user => ({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });
  export const getUser = () => ({
    type: GET_USER_PROFILE
  });
  export const auth_failure = () => ({
    type: AUTH_FAILURE
  });
  export const getUserSuccess = user => ({
    type: GET_USER_PROFILE_SUCCESS,
    payload: user
  });
  
  export const registerUser = (user, history) => ({
    type: REGISTER_USER,
    payload: { user, history }
  });
  export const registerUserSuccess = user => ({
    type: REGISTER_USER_SUCCESS,
    payload: user
  });
  export const forgotPassword = (email) => ({
    type: FORGOT_PASSWORD,
    payload: email
  });
  export const forgotPasswordSuccess = () => ({
    type: FORGOT_PASSWORD_SUCCESS
  });

  export const logoutUser = history => ({
    type: LOGOUT_USER,
    payload: { history }
  });
  
  export const logoutUserSuccess = () => ({
    type: LOGOUT_USER_SUCCESS
  });
  