import {
    GET_CUSTOMERS,
    GET_CUSTOMERS_SUCCESS,
    ADD_CUSTOMERS,
    ADD_CUSTOMERS_SUCCESS,
    EDIT_CUSTOMERS,
    EDIT_CUSTOMERS_SUCCESS,
    DELETE_CUSTOMERS,
    DELETE_CUSTOMERS_SUCCESS
} from "../actions";

export const addcustomer = customer => ({
    type: ADD_CUSTOMERS,
    payload: customer
  });
export const addcustomerSuccess = customer => ({
    type: ADD_CUSTOMERS_SUCCESS,
    payload: customer
});
export const deletecustomer = customer => ({
    type: DELETE_CUSTOMERS,
    payload: customer
  });
export const deletecustomerSuccess = customer => ({
    type: DELETE_CUSTOMERS_SUCCESS,
    payload: customer
});
export const editcustomer = customer => ({
    type: EDIT_CUSTOMERS,
    payload: customer
  });
  export const editcustomerSuccess = customer => ({
    type: EDIT_CUSTOMERS_SUCCESS,
    payload: customer
  });
  export const getcustomer = customer => ({
    type: GET_CUSTOMERS,
    payload: customer
  });
export const getcustomerSuccess = customer => ({
    type: GET_CUSTOMERS_SUCCESS,
    payload: customer
});
