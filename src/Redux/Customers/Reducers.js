import {
    GET_CUSTOMERS,
    GET_CUSTOMERS_SUCCESS,
    ADD_CUSTOMERS,
    ADD_CUSTOMERS_SUCCESS,
    EDIT_CUSTOMERS,
    EDIT_CUSTOMERS_SUCCESS,
    DELETE_CUSTOMERS,
    DELETE_CUSTOMERS_SUCCESS
  } from "../actions";
  const INIT_STATE = {
    Customer: [],
    loading: false
  };
  
  export default (state = INIT_STATE, action) => {
    switch (action.type) {
      case GET_CUSTOMERS:
        return { ...state, loading: true, Customer: [] };
      case GET_CUSTOMERS_SUCCESS:
        return { ...state, loading: false, Customer: action.payload };
      case ADD_CUSTOMERS:
        return { ...state, loading: true };
      case ADD_CUSTOMERS_SUCCESS:
        return {
          ...state,
          loading: false,
          Customer: [...state.Customer, action.payload]
        };
      case EDIT_CUSTOMERS:
        return { ...state, loading: true };
      case EDIT_CUSTOMERS_SUCCESS:
        return {
          ...state,
          loading: false,
          Admin: [
            ...state.Customer.map(Customer =>
                Customer.id === action.payload.id
                ? {
                    
                    ...Customer,
                    first_name: action.payload.first_name,
                    last_name: action.payload.last_name,
                    phone_number: action.payload.phone_number,
                    username: action.payload.username,
                    contact_email: action.payload.contact_email,
                    id: action.payload.id,
                  }
                : Customer
            )
          ]
        };
      case DELETE_CUSTOMERS:
        return { ...state, loading: true };
      case DELETE_CUSTOMERS_SUCCESS:
        return {
          ...state,
          loading: false,
          Customer: [
            ...state.Customer.filter(Customer => {
              return Customer.id !== action.payload;
            })
          ]
        };
     
    }
  };
  