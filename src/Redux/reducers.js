import { combineReducers } from "redux";
import Customer from "./Customers/Reducers";
import Reports from "./Reports/Reducers";
import Admin from "./Admin/reducers";
const reducers = combineReducers({
    Customer,
    Reports,
    Admin
  });
  
  export default reducers;
  