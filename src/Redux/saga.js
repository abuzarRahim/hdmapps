import { all } from "redux-saga/effects";
import CustomerSaga from "./Customers/saga";
import ReportSaga from "./Reports/saga";
import AdminSaga from "./Admin/saga";
import AuthSaga from "./Auth/saga";

export default function* rootSaga(getState) {
  yield all([
    CustomerSaga(),
    ReportSaga(),
    AdminSaga(),
    AuthSaga()
  ]);
}
