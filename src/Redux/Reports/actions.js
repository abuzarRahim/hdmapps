import {
    GET_REPORTS,
    GET_REPORTS_SUCCESS,
    ADD_REPORTS,
    ADD_REPORTS_SUCCESS,
    EDIT_REPORTS,
    EDIT_REPORTS_SUCCESS,
    DELETE_REPORTS,
    DELETE_REPORTS_SUCCESS
} from "../actions";

export const addReport = report => ({
    type: ADD_REPORTS,
    payload: report
  });
export const addReportSuccess = report => ({
    type: ADD_REPORTS_SUCCESS,
    payload: report
});
export const deleteReport = report => ({
    type: DELETE_REPORTS,
    payload: report
  });
export const deleteReportSuccess = report => ({
    type: DELETE_REPORTS_SUCCESS,
    payload: report
});
export const editReport = report => ({
    type: EDIT_REPORTS,
    payload: report
  });
  export const editReportSuccess = report => ({
    type: EDIT_REPORTS_SUCCESS,
    payload: report
  });
  export const getReport = report => ({
    type: GET_REPORTS,
    payload: report
  });
export const getReportSuccess = report => ({
    type: GET_REPORTS_SUCCESS,
    payload: report
});
