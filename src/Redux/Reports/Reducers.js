import {
    GET_REPORTS,
    GET_REPORTS_SUCCESS,
    ADD_REPORTS,
    ADD_REPORTS_SUCCESS,
    EDIT_REPORTS,
    EDIT_REPORTS_SUCCESS,
    DELETE_REPORTS,
    DELETE_REPORTS_SUCCESS
  } from "../actions";
  const INIT_STATE = {
    Report: [],
    loading: false
  };
  
  export default (state = INIT_STATE, action) => {
    switch (action.type) {
      case GET_REPORTS:
        return { ...state, loading: true, Report: [] };
      case GET_REPORTS_SUCCESS:
        return { ...state, loading: false, Report: action.payload };
      case ADD_REPORTS:
        return { ...state, loading: true };
      case ADD_REPORTS_SUCCESS:
        return {
          ...state,
          loading: false,
          Report: [...state.Report, action.payload]
        };
      case EDIT_REPORTS:
        return { ...state, loading: true };
      case EDIT_REPORTS_SUCCESS:
        return {
          ...state,
          loading: false,
          Report: [
            ...state.Report.map(Report =>
                Report.Report_id === action.payload.Report_id
                ? {
                    
                    ...Report,
                    Report_type: action.payload.Report_type,
                    Report_id: action.payload.Report_id,
                    Report_name: action.payload.Report_name,
                    Report_StartDate: action.payload.Report_StartDate,
                    Report_EndDate: action.payload.Report_EndDate,

                  }
                : Report
            )
          ]
        };
      case DELETE_REPORTS:
        return { ...state, loading: true };
      case DELETE_REPORTS_SUCCESS:
        return {
          ...state,
          loading: false,
          Report: [
            ...state.Report.filter(Report => {
              return Report.Report_id !== action.payload;
            })
          ]
        };
     
    }
  };
  