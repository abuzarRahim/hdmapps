import {
    GET_ADMIN,
    GET_ADMIN_SUCCESS,
    ADD_ADMIN,
    ADD_ADMIN_SUCCESS,
    EDIT_ADMIN,
    EDIT_ADMIN_SUCCESS,
    DELETE_ADMIN,
    DELETE_ADMIN_SUCCESS
} from "../actions";

export const addcustomer = customer => ({
    type: ADD_ADMIN,
    payload: customer
  });
export const addcustomerSuccess = customer => ({
    type: ADD_ADMIN_SUCCESS,
    payload: customer
});
export const deletecustomer = customer => ({
    type: DELETE_ADMIN,
    payload: customer
  });
export const deletecustomerSuccess = customer => ({
    type: DELETE_ADMIN_SUCCESS,
    payload: customer
});
export const editcustomer = customer => ({
    type: EDIT_ADMIN,
    payload: customer
  });
  export const editcustomerSuccess = customer => ({
    type: EDIT_ADMIN_SUCCESS,
    payload: customer
  });
  export const getcustomer = customer => ({
    type: GET_ADMIN,
    payload: customer
  });
export const getcustomerSuccess = customer => ({
    type: GET_ADMIN_SUCCESS,
    payload: customer
});
