import {
    GET_ADMIN,
    GET_ADMIN_SUCCESS,
    ADD_ADMIN,
    ADD_ADMIN_SUCCESS,
    EDIT_ADMIN,
    EDIT_ADMIN_SUCCESS,
    DELETE_ADMIN,
    DELETE_ADMIN_SUCCESS
  } from "../actions";
  const INIT_STATE = {
    Admin: [],
    loading: false
  };
  
  export default (state = INIT_STATE, action) => {
    switch (action.type) {
      case GET_ADMIN:
        return { ...state, loading: true, Admin: [] };
      case GET_ADMIN_SUCCESS:
        return { ...state, loading: false, Admin: action.payload };
      case ADD_ADMIN:
        return { ...state, loading: true };
      case ADD_ADMIN_SUCCESS:
        return {
          ...state,
          loading: false,
          Admin: [...state.Admin, action.payload]
        };
      case EDIT_ADMIN:
        return { ...state, loading: true };
      case EDIT_ADMIN_SUCCESS:
        return {
          ...state,
          loading: false,
          Admin: [
            ...state.Admin.map(Admin =>
                Admin.id === action.payload.id
                ? {
                    
                    ...Admin,
                    first_name: action.payload.first_name,
                    last_name: action.payload.last_name,
                    phone_number: action.payload.phone_number,
                    username: action.payload.username,
                    contact_email: action.payload.contact_email,
                    id: action.payload.id,
                  }
                : Admin
            )
          ]
        };
      case DELETE_ADMIN:
        return { ...state, loading: true };
      case DELETE_ADMIN_SUCCESS:
        return {
          ...state,
          loading: false,
          Admin: [
            ...state.Admin.filter(Admin => {
              return Admin.id !== action.payload;
            })
          ]
        };
     
    }
  };
  