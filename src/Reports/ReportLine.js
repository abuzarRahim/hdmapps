import { Line } from "react-chartjs-2";
import React, { Component } from 'react';
import {
    CardBody,Card,Label
} from 'reactstrap';
const data={
    labels:["1","2","3","4","5"],
    datasets: [
        {
          label:"Report 1",
          fill: false,
          data: [500,100,200,50,40],
          borderColor: "#3b3cfa"
        }
      ]
}
class Lines extends Component{
 
    render()
    {
        return (
            <Card>
               <div className="Report-1">
                   <CardBody >
                       <Label style={{display:"flex",justifyContent:'center'}}>Report Type 1</Label>
                       
                   </CardBody>
                  <Card style={{width:'700px'}}>
                       <Line data={data}/>
                  </Card>
              </div>
               </Card>
        )
    }
}
export default Lines;