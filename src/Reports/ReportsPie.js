
import { Doughnut } from "react-chartjs-2";
import React, { Component } from 'react';
import axios from 'axios';
import {
    CardBody,Card,Label
} from 'reactstrap';
const getReportType = async branch_id =>
  await axios
    .get("http://localhost:8080/api/v1/get/report/modulation/types", {

     
    })
    .then(response => response.data)
    .catch(error => error.response);
var dataArr=[];
var keyArray=[]
var valArr=[];
class Pie extends Component{
  constructor(props)
  {
    super(props)
    {
      this.state={
        datas:[],
        namedata:[],
        valuedata:[]
      }
    }
  }
    
    componentDidMount()
    {

    
      //getEmailList().then(emailList => console.log(emailList))
      const data=getReportType().then(data=>this.setState({datas:data})/*data.map((email,i) => {
        dataArr.push(email)
        keyArray[i]=email.name
        valArr[i]=email.value
      })*/)

     
    this.setState({namedata:dataArr,
                  valuedata:valArr})
    }

    render()
    {
        console.log(this.state.datas)
        let valArr = [];
        let keyArr=[];
        this.state.datas.map((email,i) => {
          valArr.push(email.value)
          keyArr.push(email.name)
        })
        
      
        
        let customLabels = keyArr.map((label,index) =>`${label}: ${valArr[index]}`)
        console.log(customLabels)
        const chartdata = {
            labels: customLabels,
            datasets: [
            {
                label: "Markets Monitored",
                backgroundColor: [
                "#83ce83",
                "#959595",
                "#f96a5d",
                "#00A6B4",
                "#6800B4",
                "#bac921",
                "#eb5764",
                "#dab781",
                "#faab81",
                "#faab81",
                "#000000",
                "#3b3c3f",
                "#3b3c3f",
                ],
                data: valArr,
            },
            ],
        };
        return (
            <Card>
                <div className="Report-1">
                    <CardBody >
                        <Label style={{display:"flex",justifyContent:'center'}}>Report Type 3</Label>
      
                    </CardBody>
                    <Card style={{width:'700px'}}>
                    <Doughnut
        data={chartdata}
        options={{
          legend: { display: true, position: "right" },

          datalabels: {
            display: true,
            color: "white",
          },
          tooltips: {
            backgroundColor: "#5a6e7f",
          },
        }}
      />
                    </Card>
                </div>
                </Card>
        )
    }
}
export default Pie;