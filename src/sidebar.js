import  { Component } from 'react';
import {Link} from 'react-router-dom';
import {
    Label,
 //   Input,
 //   Col,
 //   Row,
 //   Button
}from 'reactstrap';
class Sidebar extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            
        }
    }
    componentDidMount()
    {

    }
    onDetailClicked=()=>{
        
    }
    onProfileClicked=()=>{
        
    }
    onReportsClicked=()=>{

    }
    render()
    {
        return (
                <div className="sidebarContainer">
                    <div style={{marginLeft:'10px',marginTop:'10px',fontSize:'24px',color:'#f8f8f9'}}>
                        <Label><Link style= {{color:'#f8f8f9'}} to="/Profile">Profile</Link></Label>
                    </div>
                    <hr/>
                    <div onClick={this.onDetailClicked}style={{marginLeft:'10px',marginTop:'10px',fontSize:'24px',color:'#f8f8f9'}}>
                        <Label ><Link style= {{color:'#f8f8f9'}} to="/Details">Details</Link></Label>
                    </div>
                    <hr/>
                    <div style={{marginLeft:'10px',marginTop:'10px',fontSize:'24px',color:'#f8f8f9'}}>
                        <Label><Link style= {{color:'#f8f8f9'}} to="/Report">Reports</Link></Label>
                    </div>
                    <hr/>
                </div>
        )
    }
}
export default Sidebar;