import './App.css';
import {BrowserRouter as Router,Route} from 'react-router-dom';

import Login from "./login";
import SideBar from "./sidebar";
import Navbar from "./navbar";
import Profile from "./profile";
import Details from "./Details";
import Report from "./Report";
import ReportLine from "./Reports/ReportBar";
import ReportBar from "./Reports/ReportsPie";
import { Component } from 'react';

class App extends Component {
  constructor(props)
  {
      super(props);
      this.state={
          loggedIn:false
      }
  }
  togglelog=()=>{
    
    this.setState({loggedIn:true})
  }
  render(){
    return(
<div>
      {this.state.loggedIn===false?<Login toggleLog={this.togglelog}/>:
      <Router>  
      <Navbar/>
      <SideBar/>
      <Route path="/Profile" component={Profile}></Route>
      <Route path="/Details" component={Details}></Route>
      <Route path="/Report" component={Report}></Route>
      <Route path="/Report-type-1" component={ReportLine}></Route>
      <Route path="/Report-type-2" component={ReportBar}></Route>
      
      
      </Router>}
    </div>
  
   
  );
}
}

export default App;
