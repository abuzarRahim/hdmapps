import  { Component } from 'react';
import _, { toPlainObject }  from "lodash";
import axios from 'axios';
import {
    Label,
    Col,
    Row
}from 'reactstrap';
const getUser = async branch_id =>
  await axios
    .get("http://localhost:8080/api/v1/get/user", {
    })
    .then(response => response.data)
    .catch(error => error.response);


class Profile extends Component{
    constructor(props)
  {
    super(props)
    {
      this.state={
        datas:[],

      }
    }
  }
    
    
    componentDidMount()
    {
        const data=getUser().then(data=>this.setState({datas:data}))
    }
    onDetailClicked=()=>{

    }
    onProfileClicked=()=>{

    }
    onReportsClicked=()=>{

    }
    render()
    {
        let obj1={}
        obj1=toPlainObject(this.state.datas[0]);
        console.log(obj1)
        return (
                <div className="ProfileContiner">
                <div style={{marginTop:'50px',marginLeft:'150px'}}>
                  <Row>
                      <Col >
                      <Label >Employee Id:</Label>
                      <Label>{obj1.id}</Label>
                      </Col>
                      
                  </Row>
                  <br/>
                  <hr/>
                  <Row>
                      <Col >
                      <Label>Employee Name:</Label>
                      <Label>{obj1.name}</Label> 
                        
                      </Col>
                
                  </Row>
                  <br/>
                  <hr/>
                  <Row>
                      <Col>
                      <Label> Birth Date:</Label>
                      <Label> {obj1.dob}</Label>
                       
                      </Col>
                      
                  </Row>
                  <br/>
                  <hr/>
                  <Row>
                      <Col>
                      <Label> Employee Email:</Label>
                      <Label> {obj1.email}</Label> 
                       
                      </Col>
                  </Row>
                  <br/><hr/>
                  <Row>
                      <Col>
                      <Label>Cnic:</Label>
                      <Label>{obj1.cnic}</Label>
                        
                      </Col>
                     
                  </Row>
                  </div>
                </div>
        )
    }
}
export default Profile;