import  { Component } from 'react';
import {
    Label,
    Input,
    Col,
    Row,
    Button
}from 'reactstrap';
class Login extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            username:"",
            password:""
        }
    }
    componentDidMount()
    {

    }
    render()
    {
        return (
                <div className="Container">
                <div className="Card">
                <div className="CardHeader">
                    <Label style={{marginTop:'10px',fontSize:'24px'}}>Sign In Window</Label>
                </div>
                <div style={{marginTop:'30px',fontSize:'24px',display:'flex',flexDirection:'column',justifyContent:'center'}}>
                    <Row  style={{marginLeft:'30px'}}>
                        <Col xs="2">
                            <Label>Username</Label>
                            <Input 
                                style={{fontSize:'20px',marginLeft:'5px'}}
                                placeholder="enter username"
                                type="text"
                                onChange={(e)=>{this.setState({username:e.target.value})}}
                            ></Input>
                        </Col>
                    </Row>
                    <Row style={{marginLeft:'30px',marginTop:'20px'}}>
                        <Col xs="3">
                            <Label >Password</Label>
                            <Input placeholder="*******"
                                type="Password"
                                style={{fontSize:'20px',marginLeft:'10px'}}
                                onChange={(e)=>{this.setState({password:e.target.value})}}
                            ></Input>
                        </Col>
                    </Row>
                    <Row style={{marginTop:'40px'}}>
                        <Col style={{display:'flex',justifyContent:'center'}}>
                            <Button onClick={this.props.toggleLog}>Sign In</Button>
                        </Col>
                    </Row>
                </div>
                </div> 
            </div>
        )
    }
}
export default Login;